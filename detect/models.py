from __future__ import unicode_literals

from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save

from watermark.lib import *
from watermark.models import WatermarkedImage

import numpy as np
import cv2, random, math, json, datetime

possible_location_set = {
							"1":[(0, 2), (1, 1), (1, 2)],
							"2":[(1, 1), (0, 2), (1, 2)],
							"3":[(0, 3), (1, 2), (1, 3)],
							"4":[(1, 2), (0, 3), (1, 3)],
							"5":[(1, 1), (0, 2), (1, 2)],
							"6":[(0, 2), (1, 1), (1, 2)],
							"7":[(1, 1), (2, 0), (0, 2)],
							"8":[(2, 0), (1, 1), (0, 2)],
							"9":[(0, 2), (1, 1), (2, 0)],
							"10":[(1, 1), (0, 2), (2, 0)],
							"11":[(1, 2), (2, 1), (0, 3)],
							"12":[(2, 1), (1, 2), (0, 3)],
							"13":[(1, 2), (0, 3), (2, 1)],
							"14":[(0, 3), (1, 2), (2, 1)],
							"15":[(1, 1), (2, 0), (2, 1)],
							"16":[(2, 0), (1, 1), (2, 1)],
							"17":[(1, 2), (2, 1), (2, 2)],
							"18":[(2, 1), (1, 2), (2, 2)],
						}


# Create your models here.
class Sessions(models.Model):
	watermark_keyword = models.CharField(max_length=200)
	watermarked_image = models.FileField(upload_to='assets/upload/')
	extracted_image_path = models.TextField(blank=True, default="")
	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.watermarked_image.path + " --- " + self.watermark_keyword

	class Meta:
	    verbose_name = 'Extract'
	    verbose_name_plural = 'Extracts'

@receiver(post_save, sender=Sessions)
def get_watermark(sender, instance, *args, **kwargs):
    if kwargs.get("created") :
		print "get watermark..."
		watermarked_image = WatermarkedImage.objects.get(watermark_keyword=instance.watermark_keyword)

		koch_zhao_keys = json.loads(watermarked_image.koch_zhao_keys)
		watermark_data = []
		img = cv2.imread(instance.watermarked_image.path, 0)
		max_size_blocks = min (img.shape)
		max_width_blocks = max_size_blocks / 8

		print instance.watermarked_image.path
		print img
		print img.shape

		counter = 0
		for keys in  koch_zhao_keys:
			# print "------------------------------------------"
			# print "\niterasi ke - ", counter
			block_x = (keys[0] / max_width_blocks) * 8
			block_y = (keys[0] % max_width_blocks) * 8
			chosen_block = (block_x, block_y)
			
			selected_block = []
			chosen_block_w = chosen_block[0] + 8
			chosen_block_h = chosen_block[1] + 8
			for i in range(chosen_block[0], chosen_block_w):
				curr_rows = []
				for j in range(chosen_block[1], chosen_block_h):
					curr_rows.append( img[i][j] )
				selected_block.append(curr_rows)

			blockf = np.float32(selected_block)  

			# dct-in dulu imagenya
			dst = cv2.dct(blockf)  
			# dst_i = np.int32(dst)
			selected_bands = possible_location_set[keys[1]]
			
			check_read_result = check_read(dst, selected_bands)
			result = 1
			if check_read_result:
				result = read_wm(dst, selected_bands)
			# print result
			watermark_data.append(result)

			counter = counter + 1

		# print "\nwatermarked data: \n", watermark_data
		# print "\nlength: \n", len(watermark_data)

		binary_list2 = bit_stream_to_binary_list(watermark_data)
		fixed_binary_list = clean_broken_byte(binary_list2)
		new_pixel = binary_list_to_pixel(fixed_binary_list)
		new_img = pixel_list_to_image(new_pixel, watermarked_image.shape)
		filename = '/home/ridwanfajar/Projects/my_projects/django/watermarker/assets/sessions/'+str(instance.id)+'_'+str(instance.watermark_keyword)+'_'+str(datetime.datetime.now())+'.jpg'
		cv2.imwrite(filename, new_img)
		instance.extracted_image_path = filename
		instance.save()

    return True