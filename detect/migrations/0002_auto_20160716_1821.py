# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-07-16 18:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('detect', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sessions',
            old_name='name',
            new_name='watermark_keyword',
        ),
    ]
