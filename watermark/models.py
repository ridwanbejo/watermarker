from __future__ import unicode_literals

from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save

from lib import *

import numpy as np
import cv2, random, math, datetime, json

possible_location_set = {
							"1":[(0, 2), (1, 1), (1, 2)],
							"2":[(1, 1), (0, 2), (1, 2)],
							"3":[(0, 3), (1, 2), (1, 3)],
							"4":[(1, 2), (0, 3), (1, 3)],
							"5":[(1, 1), (0, 2), (1, 2)],
							"6":[(0, 2), (1, 1), (1, 2)],
							"7":[(1, 1), (2, 0), (0, 2)],
							"8":[(2, 0), (1, 1), (0, 2)],
							"9":[(0, 2), (1, 1), (2, 0)],
							"10":[(1, 1), (0, 2), (2, 0)],
							"11":[(1, 2), (2, 1), (0, 3)],
							"12":[(2, 1), (1, 2), (0, 3)],
							"13":[(1, 2), (0, 3), (2, 1)],
							"14":[(0, 3), (1, 2), (2, 1)],
							"15":[(1, 1), (2, 0), (2, 1)],
							"16":[(2, 0), (1, 1), (2, 1)],
							"17":[(1, 2), (2, 1), (2, 2)],
							"18":[(2, 1), (1, 2), (2, 2)],
						}



# Create your models here.
class Stock(models.Model):
	name = models.CharField(max_length=200)
	resource = models.FileField(upload_to='assets/upload/')
	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.name

class Label(models.Model):
	name = models.CharField(max_length=200)
	resource = models.FileField(upload_to='assets/upload/')
	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.name

	class Meta:
		verbose_name = 'Image Label'
		verbose_name_plural = 'Image Labels'

class WatermarkedImage(models.Model):
	stock = models.ForeignKey(Stock)
	label = models.ForeignKey(Label)
	
	watermark_keyword = models.CharField(max_length=200)
	koch_zhao_keys = models.TextField(blank=True)
	image_path = models.TextField(blank=True)
	
	shape = models.IntegerField(blank=True, default=0)
	psnr = models.FloatField(blank=True, default=0.0)
	mse = models.FloatField(blank=True, default=0.0)
	
	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.stock.name + " watermarked by " + self.label.name

	class Meta:
		verbose_name = 'Embed Watermark'
		verbose_name_plural = 'Embed Watermarks'

@receiver(post_save, sender=WatermarkedImage)
def do_watermark(sender, instance, *args, **kwargs):
    if kwargs.get("created") :
		print instance.label.resource.path
		print instance.stock.resource.path

		im = cv2.imread(instance.label.resource.path, 0)
		width, height = im.shape

		binary_list = pixel_to_binary_list(im)
		bit_stream = binary_list_to_bit_stream(binary_list)
		sample_data = bit_stream
		chosen_blocks = []
		koch_zhao_keys = []

		img = cv2.imread(instance.stock.resource.path, 0)
		max_size_blocks = min (img.shape)
		max_width_blocks = max_size_blocks / 8
		available_blocks = max_width_blocks * max_width_blocks
		block_lists = range(0, available_blocks)
		
		msg_len = 0
		operate = True
		while operate:
			if msg_len < len(sample_data):
				dct_block = random.choice(block_lists)
					
				try:
					block_x = (dct_block / max_width_blocks) * 8
					block_y = (dct_block % max_width_blocks) * 8
					chosen_block = (block_x, block_y)

					selected_block = []
					chosen_block_w = chosen_block[0] + 8
					chosen_block_h = chosen_block[1] + 8
					for i in range(chosen_block[0], chosen_block_w):
						curr_rows = []
						for j in range(chosen_block[1], chosen_block_h):
							curr_rows.append( img[i][j] )
						selected_block.append(curr_rows)

					blockf = np.float32(selected_block)  

					# dct-in dulu imagenya
					dst = cv2.dct(blockf)  
					
					# sisipin watermark pakai algoritma koch zhao
					ci = sample_data[msg_len]

					selected_pattern = ""
					for location in possible_location_set:
						bands = possible_location_set[location]
						if check_write(dst, bands, ci):
							selected_pattern = location
							break
				
					if selected_pattern != "":
						block_lists.remove(block_lists.index(dct_block))
						selected_bands = possible_location_set[selected_pattern]
						new_dst = write_wm(dst, selected_bands, ci)
						
						# idct-in hasil penyisipan
						new_block = cv2.idct(new_dst)
						
						# timpa ke gambar asalnya
						for i in range(0,8):
							for j in range(0,8):
								# img[chosen_block[0]+i][chosen_block[1]+j] = np.int32(new_block[i][j])
								img[chosen_block[0]+i][chosen_block[1]+j] = new_block[i][j]
					
						chosen_blocks.append(dct_block)
						koch_zhao_keys.append((dct_block, selected_pattern))
						msg_len = msg_len + 1
					else:
						continue
				except Exception, e:
					print msg_len,
					print "Error penghapusan index: ", e
					continue
			
			elif msg_len == len(sample_data):
				operate = False

		print "\nunused block: ",len(block_lists)
		print "\nchosen block: ", chosen_blocks
		print "\nlen chosen block: ", len(chosen_blocks)
		print "\nkunci watermark: \n", koch_zhao_keys
		print "length: ", len(koch_zhao_keys)
		filename = '/home/ridwanfajar/Projects/my_projects/django/watermarker/assets/watermarked/'+instance.stock.name+'_'+str(datetime.datetime.now())+'.jpg'
		cv2.imwrite(filename,img)

		instance.image_path = filename
		instance.koch_zhao_keys = json.dumps(koch_zhao_keys)
		instance.shape = width
		instance.save()

    return True