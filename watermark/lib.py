import numpy as np
import cv2, math, random

Q = 10
D = 0

Q = np.array([
					[16.0, 11.0, 10.0, 16.0, 24.0, 40.0, 51.0, 61.0],
					[12.0, 12.0, 14.0, 19.0, 26.0, 58.0, 60.0, 55.0],
					[14.0, 13.0, 16.0, 24.0, 40.0, 57.0, 69.0, 56.0],
					[14.0, 17.0, 22.0, 29.0, 51.0, 87.0, 80.0, 62.0],
					[18.0, 22.0, 37.0, 56.0, 68.0, 109.0, 103.0, 77.0],
					[24.0, 35.0, 55.0, 64.0, 81.0, 104.0, 113.0, 92.0],
					[49.0, 64.0, 78.0, 87.0, 103.0, 121.0, 120.0, 101.0],
					[72.0, 92.0, 95.0, 98.0, 112.0, 100.0, 103.0, 99.0]
				])

def zero_filler(filler_len):
	filler = ""
	for x in range(0, filler_len):
		filler = filler + "0"

	return filler

def pixel_to_binary_list (im):
	width, height = im.shape
	binary_list = []
	for x in range(0, width):
		for y in range(0, height):
			pix = bin(int(hex((im[x][y])), 16))
			pix = str(pix)
			temp_pix = pix.split('0b')
			len_temp_pix = 8 - len(temp_pix[1])
			new_binary = zero_filler(len_temp_pix) + temp_pix[1]
			binary_list.append(new_binary)
	return binary_list

def binary_list_to_bit_stream(binary_list):
	bit_stream = []
	for pixel_byte in binary_list:
		for bit in pixel_byte:
			bit_stream.append(int(bit))
	return bit_stream

def bit_stream_to_binary_list(bit_stream):
	binary_list = []
	temp_binary = []
	counter = 0
	i = 0
	for bit in bit_stream:
		if counter < 7:
			# print i
			temp_binary.append(str(bit))
			counter = counter + 1
		else:
			# print counter
			# print i
			temp_binary.append(str(bit))
			new_binary = "".join(temp_binary)
			binary_list.append(new_binary)
			counter = 0
			temp_binary = []
		i = i + 1
	# print i
	return binary_list

def clean_broken_byte(binary_list):
	# print "clean broken byte"
	new_binary_list = []
	for pixel_byte in binary_list:
		if pixel_byte.find('-1') == -1:
			new_binary_list.append(pixel_byte)
		else:
			new_binary_list.append('11111111')
			# new_binary_list.append('00000000')

	return new_binary_list

def binary_list_to_pixel(binary_list):
	new_pixel = []
	zero_byte = 0
	for pixel_byte in binary_list:
		temp_byte = list(pixel_byte)
		
		# print "==> ", temp_byte

		for bit in pixel_byte:
			if bit == '1':
				break
			temp_byte.pop(0)
			zero_byte = zero_byte + 1
		
		# print "==@ ", temp_byte

		if zero_byte != 8:
			new_pixel.append(int("".join(temp_byte), 2))
		else:
			new_pixel.append(int("00000000", 2))
		
		zero_byte = 0

	return new_pixel

def pixel_list_to_image(pixel, dimension):
	counter = 0
	temp_pixel = []
	new_img = []
	for pix in pixel:
		if counter < dimension - 1:
			temp_pixel.append(pix)
			counter = counter + 1
		else:
			temp_pixel.append(pix)
			new_img.append(temp_pixel)
			counter = 0
			temp_pixel = []

	return np.array(new_img)

def check_write(dst_i, bands, ci):
	yq1 = dst_i[bands[0][0]][bands[0][1]] / Q[bands[0][0]][bands[0][1]]
	yq2 = dst_i[bands[1][0]][bands[1][1]] / Q[bands[1][0]][bands[1][1]]
	yq3 = dst_i[bands[2][0]][bands[2][1]] / Q[bands[2][0]][bands[2][1]]

	# print yq1, yq2, yq3

	if ( abs(yq1) == 0.0 and abs(yq2) == 0.0 and abs(yq3) == 0.0 ):
		return False
	else:
		if ci == 1:
			if ( min(abs(yq1), abs(yq2)) + D < abs(yq3) ):
				# print "False in 1"
				return False
			else:
				# print "True in 1"
				return True
		elif ci == 0:
			if ( max(abs(yq1), abs(yq2)) > abs(yq3) + D ):
				# print "False in 0"
				return False
			else:
				# print "True in 0"
				return True

def write_wm(dst_i, bands, ci):
	yq1 = dst_i[bands[0][0]][bands[0][1]] / Q[bands[0][0]][bands[0][1]]
	yq2 = dst_i[bands[1][0]][bands[1][1]] / Q[bands[1][0]][bands[1][1]]
	yq3 = dst_i[bands[2][0]][bands[2][1]] / Q[bands[2][0]][bands[2][1]]

	# print "\nbefore:\n"
	# print yq1, yq2, yq3

	if ci == 1:
		if ( (yq1 > yq3 + D) == True ) and ( (yq2 > yq3 + D) == True ):
			pass
		else:
			temp_bands = [yq1, yq2, yq3]
			temp_yq3 = min(temp_bands)
			temp_yq3_idx = temp_bands.index(temp_yq3)
			
			if temp_yq3_idx == 0:
				# print "y1 harus dituker di 1"
				temp_yq = yq1
				yq1 = yq3
				yq3 = temp_yq

			elif temp_yq3_idx == 1:
				# print "y2 harus dituker di 1"
				temp_yq = yq2
				yq2 = yq3
				yq3 = temp_yq

	elif ci == 0:
		if ( (yq1 + D < yq3) == True ) and ( (yq2 + D < yq3) == True ):
			pass
		else:
			temp_bands = [yq1, yq2, yq3]
			temp_yq3 = max(temp_bands)
			temp_yq3_idx = temp_bands.index(temp_yq3)

			if temp_yq3_idx == 0:
				# print "y1 harus dituker di 0"
				temp_yq = yq1
				yq1 = yq3
				yq3 = temp_yq

			elif temp_yq3_idx == 1:
				# print "y2 harus dituker di 0"
				temp_yq = yq2
				yq2 = yq3
				yq3 = temp_yq

	# print "\nafter:\n"
	# print yq1, yq2, yq3

	dst_i[bands[0][0]][bands[0][1]] = yq1 * Q[bands[0][0]][bands[0][1]]
	dst_i[bands[1][0]][bands[1][1]] = yq2 * Q[bands[1][0]][bands[1][1]] 
	dst_i[bands[2][0]][bands[2][1]] = yq3 * Q[bands[2][0]][bands[2][1]]

	return dst_i

def check_read(dst_i, bands):
	yq1 = dst_i[bands[0][0]][bands[0][1]] / Q[bands[0][0]][bands[0][1]]
	yq2 = dst_i[bands[1][0]][bands[1][1]] / Q[bands[1][0]][bands[1][1]]
	yq3 = dst_i[bands[2][0]][bands[2][1]] / Q[bands[2][0]][bands[2][1]]

	temp_bands = [yq1, yq2, yq3]

	# print "\nlocated bit:\n"
	# print yq1, yq2, yq3

	if ( (abs(yq1) == max(temp_bands)) and (abs(yq2) == min(temp_bands)) ) or ( (abs(yq1) == min(temp_bands)) and (abs(yq2) == max(temp_bands)) ):
		# print "False"
		return False

	return True

def read_wm(dst_i, bands):
	yq1 = dst_i[bands[0][0]][bands[0][1]] / Q[bands[0][0]][bands[0][1]]
	yq2 = dst_i[bands[1][0]][bands[1][1]] / Q[bands[1][0]][bands[1][1]]
	yq3 = dst_i[bands[2][0]][bands[2][1]] / Q[bands[2][0]][bands[2][1]]

	# print "\nselected bands:\n"
	# print yq1, yq2, yq3

	if ((yq1 > yq2 + D) == True) and ((yq2 > yq3 + D) == True):
		return 1			
	if ((yq1 + D < yq3) == True) and ((yq2 + D < yq3) == True):
		return 0

	return 1
