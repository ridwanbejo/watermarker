from django.contrib import admin
from watermark.models import *

# Register your models here.
admin.site.register(Stock)
admin.site.register(Label)

class WatermarkedImageAdmin(admin.ModelAdmin):
	list_display = ['stock', 'label', 'watermark_keyword']
	exclude = ('koch_zhao_keys',)

admin.site.register(WatermarkedImage, WatermarkedImageAdmin)